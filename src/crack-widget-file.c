#include <glib.h>
#include <gmodule.h>
#include <gtk/gtk.h>
#include <libxml/globals.h>
#include <libxml/parser.h>
#include <gconf/gconf-client.h>
#include <gconf/gconf-value.h>
#include "crack-widget.h"

static GConfClient *client;

struct CrackWidgetFile_
{
	gchar* key;
	gchar* label;
};

typedef struct CrackWidgetFile_ CrackWidgetFile;

static const gchar* parse_filename (const gchar* filename)
{
	return (g_strrstr (filename, G_DIR_SEPARATOR_S) + 1); /* um..... */
}

void
crack_file_notify (GConfClient *client, guint cnxn_id, GConfEntry *entry, gpointer data)
{
	gtk_label_set_text (GTK_LABEL (data),
			parse_filename (gconf_value_get_string
			(gconf_entry_get_value (entry))));
}

void
crack_file_cb (GtkWidget *widget, gpointer data)
{
	GConfValue *value;
	GtkWidget *dialog;
	
	dialog = gtk_file_selection_new ("Select file");
	value = gconf_client_get (client, (gchar*)data, NULL);
	gtk_file_selection_set_filename (GTK_FILE_SELECTION (dialog),
			gconf_value_get_string (value));
	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_OK)
	{
		value = gconf_value_new (GCONF_VALUE_STRING);
		gconf_value_set_string (value,
			gtk_file_selection_get_filename (GTK_FILE_SELECTION (dialog)));
		gconf_client_set (client, (gchar*)data, value, NULL);
	}
	gtk_widget_destroy (dialog);
}

gpointer
crack_file_init_from_xml (xmlNodePtr cur, gchar* key)
{
	CrackWidgetFile *myself;
	gchar *label;
	
	label = xmlGetProp (cur, "label");
	for (; cur != NULL; cur = cur->next);
	
	if (label != NULL)
	{
		myself = g_malloc (sizeof (CrackWidgetFile));
		myself->key = key;
		myself->label = label;
		return myself;
	}
	else return NULL;
}

gint
crack_file_get_height (gpointer data)
{
	return 1;
}

void
crack_file_create_widgets (gpointer data, GtkTable *table, gint y)
{
	CrackWidgetFile* myself;
	GtkWidget *label;
	GtkWidget *file;
	GtkWidget *browse;
	GConfValue *value;
	
	myself = (CrackWidgetFile*) data;
	
	label = gtk_label_new (g_strconcat (myself->label, ":", NULL));
	file = gtk_label_new ("");
	browse = gtk_button_new_with_label ("Browse...");
	
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label, 1, 2, y, y + 1,
				GTK_FILL, GTK_FILL, 6, 3);
	gtk_table_attach (GTK_TABLE (table), file, 2, 5, y, y + 1,
				GTK_FILL, GTK_FILL, 6, 3);
	gtk_table_attach (GTK_TABLE (table), browse, 5, 6, y, y + 1,
				GTK_FILL, GTK_FILL, 5, 6);

	value = gconf_client_get (client, myself->key, NULL);
	gtk_label_set_text (GTK_LABEL (file), parse_filename (gconf_value_get_string (value)));
	gconf_client_notify_add (client, myself->key, (GConfClientNotifyFunc)
		crack_file_notify, file, NULL, NULL);
	g_signal_connect (browse, "clicked", (GCallback)crack_file_cb, myself->key);
	gtk_widget_show (label);
	gtk_widget_show (file);
	gtk_widget_show (browse);
}

CrackWidget*
crack_widget_file_init ()
{
	CrackWidget *widget;
	client = gconf_client_get_default ();
	
	widget = crack_widget_new ();
	widget->tag = g_strdup ("file");
	widget->get_xml_tagname = NULL; /* internal */
	widget->init_from_xml = &crack_file_init_from_xml;
	widget->get_height = &crack_file_get_height;
	widget->create_widgets = &crack_file_create_widgets;
	return widget;
}
