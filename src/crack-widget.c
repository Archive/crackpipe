#include "crack-widget.h"

CrackWidget*
crack_widget_new ()
{
	CrackWidget *widget;
	widget = g_malloc (sizeof (CrackWidget));
	widget->tag = NULL;
	widget->file = NULL;
	widget->location = NULL;
	widget->handle = NULL;
}

void
crack_widget_deep_free (CrackWidget *widget)
{
	if (widget->handle)
		g_module_close (widget->handle);
	if (widget->file)
		g_free (widget->file);
	if (widget->location)
		g_free (widget->location);
	if (widget->tag)
		g_free (widget->tag);
	g_free (widget);
}
