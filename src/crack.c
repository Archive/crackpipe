#include <libxml/globals.h>
#include <libxml/parser.h>
#include <glib.h>
#include <gtk/gtk.h>
#include <gconf/gconf-client.h>
#include <gconf/gconf-value.h>
#include <libgnomevfs/gnome-vfs.h>
#include "crack-widget-manager.h"

struct _Section {
	gchar *name;
	gint subcount;
	GList *subsections;
	gint keycount;
	gint ycount;
};

struct _SubSection {
	gchar *name;
	GList *keys;
};

struct _Key {
	gchar *key;
	CrackWidgetInstance *widget;
};

typedef struct _Section Section;
typedef struct _SubSection SubSection;
typedef struct _Key Key;

GList *sections = NULL;
GConfClient *client;
GtkWidget *main_container;
GtkWidget *main_frame;
GtkWidget *current_page;

static void parseKey (xmlDocPtr doc, xmlNodePtr cur, Section *section, SubSection *sub)
{	
	Key *key;
	CrackWidgetInstance *widget;
	
	key = g_malloc (sizeof (Key));
	
	key->key = xmlGetProp (cur, "name");
	key->widget = NULL;
	
	cur = cur->xmlChildrenNode;
	while ((cur != NULL) && (key->widget == NULL))
	{
		key->widget=crack_widget_init_from_xml (cur, key->key, (gchar*) cur->name);
		if (cur)
			cur = cur->next;
	}
	
	if (key->widget != NULL)
	{
		sub->keys = g_list_append (sub->keys, key);
		section->keycount++;
		section->ycount += key->widget->height;
	} else {
		g_free (key->key);
		g_free (key);
	}
}

static void parseSub (xmlDocPtr doc, xmlNodePtr cur, Section *section)
{
	xmlChar *title;
	SubSection *sub;
	
	title = xmlGetProp (cur, "title");
	sub = g_malloc (sizeof (SubSection));
	sub->name = title;
	sub->keys = NULL;
	section->subsections = g_list_append
		(section->subsections, sub);
	section->subcount++;
	
	cur = cur->xmlChildrenNode;
	while (cur != NULL)
	{
		if ((!xmlStrcmp (cur->name, (const xmlChar*)"key")))
			parseKey (doc, cur, section, sub);
		
		cur = cur->next;
	}
	
}

static void parseSection (xmlDocPtr doc, xmlNodePtr cur)
{
	xmlChar *title;
	GList *s;
	Section *section;
	SubSection *default_sub = NULL;
	
	title = xmlGetProp (cur, "title");
	section = g_malloc (sizeof (Section));
	section->name = title;
	section->subsections = NULL;
	section->subcount = 0;
	section->keycount = 0;
	section->ycount = 0;
	sections = g_list_append (sections, section);
	
	cur = cur->xmlChildrenNode;
	while (cur != NULL)
	{
		if ((!xmlStrcmp (cur->name, (const xmlChar*)"key")))
		{
			if (default_sub == NULL)
			{
				default_sub = g_malloc (sizeof (SubSection));
				default_sub->name = g_strdup ("__default");
				default_sub->keys = NULL;
				section->subsections = g_list_append
					(section->subsections, default_sub);
				section->subcount++;
			}
			parseKey (doc, cur, section, default_sub);
		}
		if ((!xmlStrcmp (cur->name, (const xmlChar*)"subsection")))
			parseSub (doc, cur, section);
		
		cur = cur->next;
	}
}

static void parseXML (gchar *docname)
{
	xmlDocPtr doc;
	xmlNodePtr cur;
	
	doc = xmlParseFile (docname);
	
	if (doc == NULL)
	{
		g_print ("Error in parsing xml\n");
		return;
	}
	
	cur = xmlDocGetRootElement (doc);
	
	if (cur == NULL)
	{
		g_print ("Empty XML\n");
		return;
	}
	
	if (xmlStrcmp(cur->name, (const xmlChar*) "crack"))
	{
		g_print ("Unrecognised document\n");
		xmlFreeDoc (doc);
		return;
	}
	
	cur = cur->xmlChildrenNode;
	while (cur != NULL)
	{
		if ((!xmlStrcmp (cur->name, (const xmlChar*)"section")))
		{
			parseSection (doc, cur);
		}
		cur = cur->next;
	}
}


static void
destroy_cb (GtkWidget *widget, gpointer data)
{
	gtk_main_quit ();
}

static void
switch_page_cb (GtkTreeSelection *selection, gpointer data)
{
	GtkWidget *table;
	GtkTreeModel *model;
	GtkTreeIter iter;
	gchar *label;
	
	gtk_widget_hide (current_page);
	
	if (gtk_tree_selection_get_selected (selection, &model, &iter))
	{
		gtk_tree_model_get (model, &iter, 0, &table, 1, &label, -1);
		current_page = table;
		gtk_widget_show (table);
	}
}

static void prepareGUI ()
{
	GtkWidget *window;
	GtkWidget *vbox;
	GtkWidget *hbox;
	GtkWidget *page_box;
	GtkListStore *list_store;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkTreeSelection *select;
	GtkWidget *list;
	GtkWidget *buttons;
	GtkWidget *quit;
	GList *i, *j, *k;
	gint w = 0, h = 0;
	
	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title (GTK_WINDOW (window), "Crackpipe");
	gtk_window_set_resizable (GTK_WINDOW (window), FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (window), 12);
	vbox = gtk_vbox_new (FALSE, 6);
	hbox = gtk_hbox_new (FALSE, 6);
	main_frame = gtk_frame_new (NULL);
	page_box = gtk_hbox_new (0, FALSE);
	
	gtk_frame_set_shadow_type (GTK_FRAME (main_frame), GTK_SHADOW_NONE);
	
	list_store = gtk_list_store_new (2, GTK_TYPE_TABLE, G_TYPE_STRING);
	list = gtk_tree_view_new_with_model (GTK_TREE_MODEL (list_store));
	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes ("", renderer, "text", 1,
								NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (list), column);
	gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (list), FALSE);
	select = gtk_tree_view_get_selection (GTK_TREE_VIEW (list));
	gtk_tree_selection_set_mode (select, GTK_SELECTION_SINGLE);
	g_signal_connect (G_OBJECT (select), "changed", G_CALLBACK (switch_page_cb), 
									NULL);
	
	buttons = gtk_hbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (buttons), GTK_BUTTONBOX_END);
	quit = gtk_button_new_from_stock (GTK_STOCK_CLOSE);

	gtk_container_add (GTK_CONTAINER (main_frame), page_box);
	gtk_box_pack_start (GTK_BOX (hbox), list, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), main_frame, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (buttons), quit, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), buttons, FALSE, FALSE, 0);
	gtk_container_add (GTK_CONTAINER (window), vbox);
	
	g_signal_connect (G_OBJECT (window), "destroy", G_CALLBACK (destroy_cb), NULL);
	g_signal_connect (G_OBJECT (quit), "clicked", G_CALLBACK (destroy_cb), NULL);
	
	for (i = sections; i != NULL; i = i->next) /* section iter */
	{
		GtkWidget *table;
		GtkTreeIter iter;
		gint y = 0;
		GtkRequisition req;
		Section *section;
		
		section = (Section*)(i->data);
		table = gtk_table_new (section->ycount + section->subcount, 6, FALSE);

		for (j = section->subsections; j != NULL; j = j->next)
		{
			SubSection *sub;
			GtkWidget *sect_label;
			sub = (SubSection*)(j->data);

			sect_label = gtk_label_new (NULL);
			if (strcmp (sub->name, "__default"))
			{
				gtk_label_set_markup (GTK_LABEL (sect_label),
					g_strconcat ("<b>", sub->name, "</b>", NULL));
			}
			else {
				gtk_label_set_markup (GTK_LABEL (sect_label),
					g_strconcat ("<b>", section->name, "</b>", NULL));
			}
			gtk_misc_set_alignment (GTK_MISC (sect_label), 0.0, 0.5);
			gtk_table_attach (GTK_TABLE (table), sect_label, 0, 6, y, y + 1,
				GTK_FILL, GTK_FILL, 0, 0);
			gtk_widget_show (sect_label);
			y++;
			
			/* HIG */
			sect_label = gtk_label_new ("  ");
			gtk_table_attach (GTK_TABLE (table), sect_label, 0, 1, y, y + 1,
				GTK_FILL, GTK_FILL, 6, 3);
			gtk_widget_show (sect_label);
			
			for (k = sub->keys; k != NULL; k = k->next) /* key iter */
			{
				Key *key;
				key = (Key*)(k->data);
				
				crack_widget_create (key->widget, GTK_TABLE (table), y);
				y += key->widget->height;
			}
		}

		gtk_widget_size_request (table, &req);
		w = MAX (w, req.width);
		h = MAX (h, req.height);
		
		gtk_list_store_append (list_store, &iter);
		gtk_list_store_set (list_store, &iter, 0, table, 1, section->name, -1);
		gtk_box_pack_start (GTK_BOX (page_box), table, TRUE, TRUE, 0);
		if (i == sections)
		{
			current_page = table;
			gtk_widget_show (table);
		}
	}
	
	gtk_widget_set_size_request (page_box, w, h);
	
	gtk_widget_show (hbox);
	gtk_widget_show (vbox);
	gtk_widget_show (main_frame);
	gtk_widget_show (page_box);
	gtk_widget_show (list);
	gtk_widget_show (buttons);
	gtk_widget_show (quit);
	gtk_widget_show (window);
}

int main (int argc, char* argv[])
{
	gtk_init (&argc, &argv);
	gnome_vfs_init ();
	crack_widgets_load ();
	
	client = gconf_client_get_default ();
	gconf_client_add_dir (client, "/", GCONF_CLIENT_PRELOAD_NONE, NULL);
	
	parseXML (g_strconcat (PACKAGE_DATA_DIR, "/crackpipe/crack.xml", NULL));
	prepareGUI ();
	gtk_main ();
}
