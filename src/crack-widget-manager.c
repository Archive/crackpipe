#include "crack-widget.h"
#include "crack-widget-manager.h"
#include <libgnomevfs/gnome-vfs.h>

/* defs from internals */
CrackWidget* crack_widget_checkbox_init ();
CrackWidget* crack_widget_file_init ();
CrackWidget* crack_widget_entry_init ();
CrackWidget* crack_widget_image_init ();

static GList* widgets = NULL;

static void
load_plugins ()
{
	GnomeVFSDirectoryHandle *handle;
	GnomeVFSFileInfo *info;
	CrackWidget *plugin;
	
	gnome_vfs_directory_open (&handle, PACKAGE_LIB_DIR,
		GNOME_VFS_FILE_INFO_FOLLOW_LINKS);
	info = gnome_vfs_file_info_new ();
	while (gnome_vfs_directory_read_next (handle, info) == GNOME_VFS_OK)
	{
		if (!g_str_has_suffix (info->name, G_MODULE_SUFFIX))
			continue;
		
		plugin = crack_widget_new ();
		plugin->handle = g_module_open (g_build_path (G_DIR_SEPARATOR_S,
			PACKAGE_LIB_DIR, info->name, NULL), 0);
		plugin->file = g_strdup (info->name);
		plugin->location = g_strdup (PACKAGE_LIB_DIR);
		
		if (!g_module_symbol (plugin->handle, "get_xml_tagname",
			(gpointer*)&plugin->get_xml_tagname))
		{
			g_warning ("%s is an invalid plugin, skipping\n", plugin->file);
			crack_widget_deep_free (plugin);
			continue;
		}
		if (!g_module_symbol (plugin->handle, "init_from_xml",
			(gpointer*)&plugin->init_from_xml))
		{
			g_warning ("%s is an invalid plugin, skipping\n", plugin->file);
			crack_widget_deep_free (plugin);
			continue;
		}
		if (!g_module_symbol (plugin->handle, "get_height",
			(gpointer*)&plugin->get_height))
		{
			g_warning ("%s is an invalid plugin, skipping\n", plugin->file);
			crack_widget_deep_free (plugin);
			continue;
		}
		if (!g_module_symbol (plugin->handle, "create_widgets",
			(gpointer*)&plugin->create_widgets))
		{
			g_warning ("%s is an invalid plugin, skipping\n", plugin->file);
			crack_widget_deep_free (plugin);
			continue;
		}
		
		plugin->tag = plugin->get_xml_tagname();
		widgets = g_list_prepend (widgets, plugin);
		
		g_log (G_LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Handler for <%s> (%s)\n",
			plugin->tag, plugin->file);
		
		gnome_vfs_file_info_clear (info);
	}
}

static void
load_internals ()
{
	CrackWidget *widget;

	widget = crack_widget_checkbox_init ();
	g_log (G_LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Handler for <%s> (internal)\n",
			widget->tag);
	widgets = g_list_prepend (widgets, widget);	

	widget = crack_widget_entry_init ();
	g_log (G_LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Handler for <%s> (internal)\n",
			widget->tag);
	widgets = g_list_prepend (widgets, widget);

	widget = crack_widget_file_init ();
	g_log (G_LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Handler for <%s> (internal)\n",
			widget->tag);
	widgets = g_list_prepend (widgets, widget);	
	
	widget = crack_widget_image_init ();
	g_log (G_LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Handler for <%s> (internal)\n",
			widget->tag);
	widgets = g_list_prepend (widgets, widget);	
}

void
crack_widgets_load ()
{
	load_internals ();
	load_plugins ();
}

CrackWidgetInstance*
crack_widget_init_from_xml (xmlNodePtr cur, gchar* key, gchar* xml_type)
{
	GList *w;
	CrackWidgetInstance* instance;
	
	for (w = widgets; w != NULL; w = w->next)
		if (!strcmp (((CrackWidget*)(w->data))->tag, xml_type))
			break;
	if (w == NULL)
		return NULL;
	
	instance = g_malloc (sizeof (CrackWidgetInstance));
	instance->widget = (CrackWidget*)(w->data);
	instance->priv_data = instance->widget->init_from_xml (cur, key);
	instance->height = instance->widget->get_height (instance->priv_data);
	return instance;
}

void
crack_widget_create (CrackWidgetInstance *instance, GtkTable *table, gint y)
{
	instance->widget->create_widgets (instance->priv_data, table, y);
}
