#ifndef __CRACK_WIDGET_H__
#define __CRACK_WIDGET_H__

#include <glib.h>
#include <gmodule.h>
#include <libxml/globals.h>
#include <gtk/gtk.h>

struct _CrackWidget
{
	gchar *tag;

	/* these 3 only valid for plugins */
	gchar	*file;
	gchar	*location;
	GModule	*handle;

	/* get_xml_tagname is *not* needed for internal widgets that create their own
	structure (and populate widget->tag) */
	gchar*		(*get_xml_tagname)	();
	gpointer	(*init_from_xml)	(xmlNodePtr cur, gchar* key);
	gint		(*get_height)		(gpointer data);
	void		(*create_widgets)	(gpointer data, GtkTable *table, gint y);
};

typedef struct _CrackWidget CrackWidget;

CrackWidget* crack_widget_new ();
void crack_widget_deep_free (CrackWidget* widget);

#endif /* __CRACK_WIDGET_H__ */
