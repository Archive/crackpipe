#include <glib.h>
#include <gmodule.h>
#include <gtk/gtk.h>
#include <libxml/globals.h>
#include <libxml/parser.h>
#include <gconf/gconf-client.h>
#include <gconf/gconf-value.h>
#include "crack-widget.h"

static GConfClient *client;

struct CrackWidgetCheckbox_
{
	gchar* key;
	gchar* label;
};

typedef struct CrackWidgetCheckbox_ CrackWidgetCheckbox;

void
check_notify (GConfClient *client, guint cnxn_id, GConfEntry *entry, gpointer data)
{
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (data),
				gconf_value_get_bool (gconf_entry_get_value (entry)));
}

void
check_cb (GtkWidget *widget, gpointer data)
{
	GConfValue *value;
	
	value = gconf_value_new (GCONF_VALUE_BOOL);
	gconf_value_set_bool (value,
			gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (widget)));
	gconf_client_set (client, (gchar*)data, value, NULL);
}

gpointer
crack_check_init_from_xml (xmlNodePtr cur, gchar* key)
{
	CrackWidgetCheckbox *myself;
	gchar *label;
	
	label = xmlGetProp (cur, "label");
	for (; cur != NULL; cur = cur->next);
	
	if (label != NULL)
	{
		myself = g_malloc (sizeof (CrackWidgetCheckbox));
		myself->key = key;
		myself->label = label;
		return myself;
	}
	else return NULL;
}

gint
crack_check_get_height (gpointer data)
{
	return 1;
}

void
crack_check_create_widgets (gpointer data, GtkTable *table, gint y)
{
	CrackWidgetCheckbox *myself;
	GtkWidget *check;
	GConfValue *value;
	
	myself = (CrackWidgetCheckbox*) data;
	
	check = gtk_check_button_new_with_label (myself->label);
	gtk_table_attach (GTK_TABLE (table), check, 1, 2, y, y + 1,
				GTK_FILL, GTK_FILL, 6, 3);
				
	value = gconf_client_get (client, myself->key, NULL);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check),
							gconf_value_get_bool (value));
	gconf_client_notify_add (client, myself->key, (GConfClientNotifyFunc)check_notify, 
					check, NULL, NULL);
	g_signal_connect (check, "toggled", (GCallback)check_cb, myself->key);
	gtk_widget_show (check);
}

CrackWidget*
crack_widget_checkbox_init ()
{
	CrackWidget *widget;
	client = gconf_client_get_default ();
	
	widget = crack_widget_new ();
	widget->tag = g_strdup ("checkbox");
	widget->get_xml_tagname = NULL; /* internal */
	widget->init_from_xml = &crack_check_init_from_xml;
	widget->get_height = &crack_check_get_height;
	widget->create_widgets = &crack_check_create_widgets;
	return widget;
}
