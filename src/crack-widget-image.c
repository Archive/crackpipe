#include <glib.h>
#include <gmodule.h>
#include <gtk/gtk.h>
#include <libxml/globals.h>
#include <libxml/parser.h>
#include <gconf/gconf-client.h>
#include <gconf/gconf-value.h>
#include "crack-widget.h"

static GConfClient *client;

struct CrackWidgetImage_
{
	gchar* key;
	gchar* label;
	GtkWidget *image;
	GdkPixbuf *pixbuf;
};

typedef struct CrackWidgetImage_ CrackWidgetImage;

static const gchar* parse_filename (const gchar* filename)
{
	return (g_strrstr (filename, G_DIR_SEPARATOR_S) + 1); /* um..... */
}

/* this function originally courtesy of Rachel Hestilow */
static GdkPixbuf*
intelligent_scale (GdkPixbuf *buf, guint scale)
{
	GdkPixbuf *scaled;
	int w, h;
	guint ow = gdk_pixbuf_get_width (buf);
	guint oh = gdk_pixbuf_get_height (buf);

	if (ow <= scale && oh <= scale)
		scaled = gdk_pixbuf_ref (buf);
	else
	{
		if (ow > oh)
		{
			w = scale;
			h = scale * (((double)oh)/(double)ow);
		}
		else
		{
			h = scale;
			w = scale * (((double)ow)/(double)oh);
		}
			
		scaled = gdk_pixbuf_scale_simple (buf, w, h, GDK_INTERP_BILINEAR);
	}
		
	return scaled;
}

void
crack_image_notify (GConfClient *client, guint cnxn_id, GConfEntry *entry, gpointer data)
{
	GdkPixbuf *new;
	CrackWidgetImage* myself;
		
	myself = (CrackWidgetImage*) data;
	
	new = gdk_pixbuf_new_from_file (gconf_value_get_string (gconf_entry_get_value 
		(entry)), NULL);
	if (new)
	{
		if (myself->pixbuf)
			g_object_unref (myself->pixbuf);
		myself->pixbuf = intelligent_scale (new, 100);
		g_object_unref (new);
		gtk_image_set_from_pixbuf (GTK_IMAGE (myself->image), myself->pixbuf);
	}
	
}

void
crack_image_cb (GtkWidget *widget, gpointer data)
{
	
	GConfValue *value;
	GtkWidget *dialog;
	
	dialog = gtk_file_selection_new ("Select file");
	value = gconf_client_get (client, (gchar*)data, NULL);
	gtk_file_selection_set_filename (GTK_FILE_SELECTION (dialog),
			gconf_value_get_string (value));
	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_OK)
	{
		value = gconf_value_new (GCONF_VALUE_STRING);
		gconf_value_set_string (value,
			gtk_file_selection_get_filename (GTK_FILE_SELECTION (dialog)));
		gconf_client_set (client, (gchar*)data, value, NULL);
	}
	gtk_widget_destroy (dialog);
}

gpointer
crack_image_init_from_xml (xmlNodePtr cur, gchar* key)
{
	CrackWidgetImage *myself;
	gchar *label;
	
	label = xmlGetProp (cur, "label");
	for (; cur != NULL; cur = cur->next);
	
	if (label != NULL)
	{
		myself = g_malloc (sizeof (CrackWidgetImage));
		myself->key = key;
		myself->label = label;
		return myself;
	}
	else return NULL;
}

gint
crack_image_get_height (gpointer data)
{
	return 1;
}

void
crack_image_create_widgets (gpointer data, GtkTable *table, gint y)
{
	CrackWidgetImage* myself;
	GdkPixbuf *pixbuf;
	GtkWidget *label;
	GtkWidget *button;
	GConfValue *value;
	
	myself = (CrackWidgetImage*) data;
	
	value = gconf_client_get (client, myself->key, NULL);
	gconf_client_notify_add (client, myself->key, (GConfClientNotifyFunc)
		crack_image_notify, myself, NULL, NULL);

	button = gtk_button_new ();
	
	label = gtk_label_new (g_strconcat (myself->label, ":", NULL));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.0);
	gtk_table_attach (GTK_TABLE (table), label, 1, 2, y, y + 1,
				GTK_FILL, GTK_FILL, 6, 3);

	pixbuf = gdk_pixbuf_new_from_file (gconf_value_get_string (value), NULL);
	if (pixbuf)
	{
		myself->pixbuf = intelligent_scale (pixbuf, 100);
		g_object_unref (pixbuf);
		myself->image = gtk_image_new_from_pixbuf (myself->pixbuf);
	}
	gtk_widget_set_size_request (myself->image, 100, 100);
	gtk_container_add (GTK_CONTAINER (button), myself->image);
	
	gtk_table_attach (GTK_TABLE (table), button, 2, 5, y, y + 1,
				GTK_FILL, GTK_FILL, 6, 3);

	g_signal_connect (button, "clicked", (GCallback)crack_image_cb, myself->key);
	
	gtk_widget_show_all (button);
	gtk_widget_show (label);
}

CrackWidget*
crack_widget_image_init ()
{
	CrackWidget *widget;
	client = gconf_client_get_default ();
	
	widget = crack_widget_new ();
	widget->tag = g_strdup ("image");
	widget->get_xml_tagname = NULL; /* internal */
	widget->init_from_xml = &crack_image_init_from_xml;
	widget->get_height = &crack_image_get_height;
	widget->create_widgets = &crack_image_create_widgets;
	return widget;
}
