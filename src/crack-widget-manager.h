#ifndef __CRACK_WIDGET_MANAGER_H__
#define __CRACK_WIDGET_MANAGER_H__

#include "crack-widget.h"

struct CrackWidgetInstance_ {
	gint height;
	gpointer priv_data;
	CrackWidget *widget;
};

typedef struct CrackWidgetInstance_ CrackWidgetInstance;

void crack_widgets_load ();
CrackWidgetInstance* crack_widget_init_from_xml (xmlNodePtr cur, gchar* key,
	gchar* xml_type);
void crack_widget_create (CrackWidgetInstance *instance, GtkTable *table, gint y);

#endif /* __CRACK_WIDGET_MANAGER_H__ */
